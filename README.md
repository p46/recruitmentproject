
Due to characteristics of task implementation was done based on java streams.

All necessary configurations can be done via <b>aplication.properties </b>file. 
To change default values of country code and limit of top fuel station brands it needs to be provided via request params.
Country codes are filtered with enum and permitted values are NO, DE and PL

Json files copy to /src/test/resources

## Gradle commands

Build project

    ./gradlew build
    
Run project

    ./gradlew bootRun 

Project build jar

    ./gradlew bootJar

Prepare docker image

    ./gradlew docker
    
        and then 
    
     docker run recruitmentproject-0.1
     
## Angular commands
install dependencies

    go to ui folder and then 

    npm install

UI run 

    To run angular ui go to ui folder and then 
    
    ng serve
    
    UI will be served under localost:4200
    
    
    
    
## Api usage

    String version of report is on endpoint 
        localhost:8080/fuel-report
    
    Json version used to comunicate with angular 
        localhost:8080/fuel-report/json