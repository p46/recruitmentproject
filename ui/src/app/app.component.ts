import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

	constructor(private http: HttpClient) { }

  title = 'test-app';

  test: FuelStationsReport;

  updateData() {
    this.fetchData()
       .subscribe(data => {
         this.test = data;
       });
  }

  fetchData(): Observable<FuelStationsReport> {

    console.log("asd")

  	const url = `http://localhost:8080/fuel-report/json`; 

  	return this.http.get<FuelStationsReport>(url)

  }
}

export class FuelStationsReport {
  country: string;
  stations: number;
  stationsWithPrices: number;
  fuelStationBrands: Map<String, number>;
}