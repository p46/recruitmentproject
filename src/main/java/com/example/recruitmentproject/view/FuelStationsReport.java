package com.example.recruitmentproject.view;

import com.example.recruitmentproject.controller.CountryCode;
import lombok.Builder;
import lombok.Getter;

import java.util.Map;

@Getter
@Builder
public class FuelStationsReport {

    private CountryCode country;

    private long stations;

    private long stationsWithPrices;

    private Map<String, Long> fuelStationBrands;

}
