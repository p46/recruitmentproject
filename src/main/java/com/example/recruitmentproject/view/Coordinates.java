
package com.example.recruitmentproject.view;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
class Coordinates {


    private Double lat;

    private Double lon;

}
