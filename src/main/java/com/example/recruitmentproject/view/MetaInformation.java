
package com.example.recruitmentproject.view;


import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
class MetaInformation {

    private String countryCode;

    private String currency;

    private String feedType;

    private String title;

    private String volumeUnit;

}
