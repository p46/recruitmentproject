
package com.example.recruitmentproject.view;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class DynamicStation {

    private String id;

    private Fuel fuel;

}
