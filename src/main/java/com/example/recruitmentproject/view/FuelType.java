package com.example.recruitmentproject.view;


import com.fasterxml.jackson.annotation.JsonValue;

public enum FuelType {

    SP95("sp95"),
    SP95_PLUS("sp95Plus"),
    SP98("sp98"),
    DIESEL("diesel"),
    DIESEL_PLUS("dieselPlus");

    private String value;

    FuelType(final String value) {
        this.value = value;
    }

    @JsonValue
    final String value() {
        return this.value;
    }

}
