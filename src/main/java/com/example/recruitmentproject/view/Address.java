
package com.example.recruitmentproject.view;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
class Address {

    private String place;

    private String postalCode;

    private String region;

    private String road;

}
