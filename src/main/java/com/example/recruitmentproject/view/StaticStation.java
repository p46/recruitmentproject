
package com.example.recruitmentproject.view;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@NoArgsConstructor
public class StaticStation {

    private String id;

    private String name;

    private Address address;

    private String brand;

    private Coordinates coordinates;

    private List<FuelType> fuels;

    private String timeZoneOffset;

    private LocalDateTime updatedAt;

}
