
package com.example.recruitmentproject.view;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor
class Fuel {

    private FuelType fuelType;

    private Double price;

    private LocalDateTime updatedAt;

}
