
package com.example.recruitmentproject.view;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor
public class DynamicFeed {

    @JsonProperty("meta")
    private MetaInformation metaInformation;

    @JsonProperty("data")
    private List<DynamicStation> stationList;

}
