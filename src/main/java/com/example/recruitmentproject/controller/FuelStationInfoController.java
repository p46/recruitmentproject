package com.example.recruitmentproject.controller;

import com.example.recruitmentproject.FuelStationInfoFetcher;
import com.example.recruitmentproject.view.DynamicFeed;
import com.example.recruitmentproject.view.FuelStationsReport;
import com.example.recruitmentproject.view.StaticFeed;

import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController()
public class FuelStationInfoController {


    private final FuelStationInfoFetcher fuelStationInfoFetcher;

    private final FuelStationProcessor fuelStationProcessor;

    public FuelStationInfoController(final FuelStationInfoFetcher fuelStationInfoFetcher, final FuelStationProcessor fuelStationProcessor) {
        this.fuelStationInfoFetcher = fuelStationInfoFetcher;
        this.fuelStationProcessor = fuelStationProcessor;
    }

    @GetMapping("/fuel-report")
    public String prepareReport(@RequestParam(defaultValue = "NO") final CountryCode countryCode,
                                @RequestParam(defaultValue = "5") final int limit) {

        StaticFeed staticFeed = fuelStationInfoFetcher.fetchStaticFeed(countryCode);
        DynamicFeed dynamicFeed = fuelStationInfoFetcher.fetchDynamicFeed(countryCode);
        Map<String, Long> stationsMap = fuelStationProcessor.getGroupedStations(dynamicFeed, staticFeed, limit);



        StringBuilder resultStringBuilder = new StringBuilder();

        resultStringBuilder.append('\n').append("Report prepared for country: ").append(countryCode).append('\n');
        resultStringBuilder.append("All stations ").append(staticFeed.getStationList().size()).append('\n');
        resultStringBuilder.append("Stations with prices ").append(dynamicFeed.getStationList().size()).append('\n');
        resultStringBuilder.append('\n');


        resultStringBuilder.append("Top ").append(limit).append(" station brands").append('\n');
        resultStringBuilder.append("Qty \t\t").append("Brand").append('\n');

        stationsMap.forEach((key, val) -> resultStringBuilder.append(val).append("\t\t\t").append(key).append('\n'));




        String result = resultStringBuilder.toString();

        System.out.println(result);

        return result;
    }

    @GetMapping("/fuel-report/json")
    public HttpEntity<FuelStationsReport> getReportAsJson(@RequestParam(defaultValue = "NO") final CountryCode countryCode,
                                                          @RequestParam(defaultValue = "5") final int limit) {

        StaticFeed staticFeed = fuelStationInfoFetcher.fetchStaticFeed(countryCode);
        DynamicFeed dynamicFeed = fuelStationInfoFetcher.fetchDynamicFeed(countryCode);
        Map<String, Long> stationsMap = fuelStationProcessor.getGroupedStations(dynamicFeed, staticFeed, limit);


        FuelStationsReport fuelStationsReport =
                FuelStationsReport.builder()
                    .country(countryCode)
                    .stations(staticFeed.getStationList().size())
                    .stationsWithPrices(dynamicFeed.getStationList().size())
                    .fuelStationBrands(stationsMap)
                .build();

        return ResponseEntity.ok(fuelStationsReport);

    }

}
