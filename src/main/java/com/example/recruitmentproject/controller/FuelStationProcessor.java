package com.example.recruitmentproject.controller;

import com.example.recruitmentproject.view.DynamicFeed;
import com.example.recruitmentproject.view.DynamicStation;
import com.example.recruitmentproject.view.StaticFeed;
import com.example.recruitmentproject.view.StaticStation;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

@Component
class FuelStationProcessor {

    Map<String, Long> getGroupedStations(final DynamicFeed dynamicFeed,
                                         final StaticFeed staticFeed,
                                         final int limit) {

            List<String> stationWithPricesList = dynamicFeed.getStationList().stream()
                    .map(DynamicStation::getId)
                    .collect(Collectors.toList());

            Map<String, Long> groupedStations = staticFeed.getStationList().stream()
                    .filter(x -> stationWithPricesList.contains(x.getId()))
                    .collect(Collectors.groupingBy(StaticStation::getBrand, Collectors.counting()))
                    .entrySet().stream()
                    .sorted((f1, f2) -> Long.compare(f2.getValue(), f1.getValue()))
                    .limit(limit)
                    .collect(Collectors.toMap(
                            Map.Entry::getKey,
                            Map.Entry::getValue,
                            (oldValue, newValue) -> oldValue, LinkedHashMap::new));

            return groupedStations;

        }

}
