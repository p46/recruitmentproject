package com.example.recruitmentproject;

import com.example.recruitmentproject.controller.CountryCode;
import com.example.recruitmentproject.view.DynamicFeed;
import com.example.recruitmentproject.view.StaticFeed;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class FuelStationInfoFetcher {

    @Value("${fuelApiLinkBase}")
    private String fuelApiLinkBase;

    @Value("${fuelApiKey}")
    private String fuelApiKey;

    @Value("${fuelApiVersion}")
    private String fuelApiVersion;

    @Value("${fuelApiCommunicationType}")
    private String fuelApiCommunicationType;

    private final RestTemplate restTemplate;

    public FuelStationInfoFetcher(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public StaticFeed fetchStaticFeed(final CountryCode countryCode) {

        String objectType = "static";

        String fullApiLink = fuelApiLinkBase + '/' + fuelApiVersion + '/' + fuelApiCommunicationType + '/' + objectType + '/' + countryCode + "?key=" + fuelApiKey;

        return restTemplate.getForObject(fullApiLink, StaticFeed.class);

    }

    public DynamicFeed fetchDynamicFeed(final CountryCode countryCode) {

        String objectType = "dynamic";

        String fullApiLink = fuelApiLinkBase + '/' + fuelApiVersion + '/' + fuelApiCommunicationType + '/' + objectType + '/' + countryCode + "?key=" + fuelApiKey;

        return restTemplate.getForObject(fullApiLink, DynamicFeed.class);

    }
}
