package com.example.recruitmentproject.controller;

import com.example.recruitmentproject.view.DynamicFeed;
import com.example.recruitmentproject.view.DynamicStation;
import com.example.recruitmentproject.view.StaticFeed;
import com.example.recruitmentproject.view.StaticStation;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class FuelStationProcessorTest {

    private FuelStationProcessor fuelStationProcessor;

    @Mock
    private DynamicFeed dynamicFeed;

    @Mock
    private StaticFeed staticFeed;

    @BeforeEach
    void setUp() {
        fuelStationProcessor = new FuelStationProcessor();
    }

    @Test
    void shouldReturnSortedStationsBrandMap() {
        //given
        createStaticFeedMocks();

        createDynamicFeedMocks();


        //when
        Map<String, Long> result = fuelStationProcessor.getGroupedStations(dynamicFeed, staticFeed, 3);

        //then
        assertThat(result)
                .isNotEmpty()
                .hasSize(2);

        assertThat(result.get("Brand2")).isEqualTo(2);
        assertThat(result.get("Brand1")).isEqualTo(1);

        assertThat(result.keySet().stream().findFirst().get()).isEqualTo("Brand2");

    }

    @Test
    void shouldReturnLimitedMap() {
        //given
        createStaticFeedMocks();

        createDynamicFeedMocks();


        //when
        Map<String, Long> result = fuelStationProcessor.getGroupedStations(dynamicFeed, staticFeed, 1);

        //then
        assertThat(result)
                .isNotEmpty()
                .hasSize(1);

        assertThat(result.get("Brand2")).isEqualTo(2);

    }

    private void createDynamicFeedMocks() {
        DynamicStation dynamicStation1 = mock(DynamicStation.class);
        when(dynamicStation1.getId()).thenReturn("id1");

        DynamicStation dynamicStation2 = mock(DynamicStation.class);
        when(dynamicStation2.getId()).thenReturn("id2");

        DynamicStation dynamicStation3 = mock(DynamicStation.class);
        when(dynamicStation3.getId()).thenReturn("id4");

        when(dynamicFeed.getStationList())
                .thenReturn(asList(dynamicStation1, dynamicStation2, dynamicStation3));

    }

    private void createStaticFeedMocks() {
        StaticStation staticStation1 = mock(StaticStation.class);
        when(staticStation1.getId()).thenReturn("id1");
        when(staticStation1.getBrand()).thenReturn("Brand1");

        StaticStation staticStation2 = mock(StaticStation.class);
        when(staticStation2.getId()).thenReturn("id2");
        when(staticStation2.getBrand()).thenReturn("Brand2");

        StaticStation staticStation3= mock(StaticStation.class);
        when(staticStation3.getId()).thenReturn("id3");

        StaticStation staticStation4 = mock(StaticStation.class);
        when(staticStation4.getId()).thenReturn("id4");
        when(staticStation4.getBrand()).thenReturn("Brand2");

        when(staticFeed.getStationList())
                .thenReturn(asList(staticStation1, staticStation2, staticStation3, staticStation4));
    }
}