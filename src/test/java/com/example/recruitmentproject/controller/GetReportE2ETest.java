package com.example.recruitmentproject.controller;

import com.example.recruitmentproject.RecruitmentprojectApplication;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.RestTemplate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;
import static org.springframework.test.web.client.ExpectedCount.once;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest(classes = RecruitmentprojectApplication.class)
class GetReportE2ETest {

    private static final String STATIC_URL = "https://api.tomtom.com/fuel/v1.0/bulk/static/NO?key=";
    private static final String DYNAMIC_URL = "https://api.tomtom.com/fuel/v1.0/bulk/dynamic/NO?key=";
    private static final String STATIC_FEED_JSON_PATH = "src/test/resources/StaticFeed-NO.json";
    private static final String DYNAMIC_FEED_JSON_PATH = "src/test/resources/DynamicFeed-NO.json";

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private MockMvc mockMvc;

    private MockRestServiceServer mockServer;

    @BeforeEach
    void setUp() throws IOException {
        mockServer = MockRestServiceServer.createServer(restTemplate);

        mockServer.expect(once(), requestTo(STATIC_URL))
                .andRespond( withSuccess(Files.readAllBytes(Paths.get(STATIC_FEED_JSON_PATH)), MediaType.APPLICATION_JSON));

        mockServer.expect(once(), requestTo(DYNAMIC_URL))
                .andRespond( withSuccess(Files.readAllBytes(Paths.get(DYNAMIC_FEED_JSON_PATH)), MediaType.APPLICATION_JSON));

    }

    @Test
    void shouldGenerateReport() throws Exception {
        //given
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/fuel-report")
                .contentType(MediaType.APPLICATION_JSON_VALUE);

        //when
        ResultActions result = this.mockMvc.perform(requestBuilder);

        //then
        mockServer.verify();

        result.andExpect(status().isOk());

        String resultString = result.andReturn().getResponse().getContentAsString();

        assertThat(resultString)
                .contains("All stations 1831")
                .contains("Stations with prices 1625")
                .contains("428\t\t\tCircle K")
                .contains("236\t\t\tEsso");
    }

    @Test
    void shouldGETReportAsJson() throws Exception {
        //given
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/fuel-report/json")
                .contentType(MediaType.APPLICATION_JSON_VALUE);

        //when
        ResultActions result = this.mockMvc.perform(requestBuilder);

        //then
        mockServer.verify();

        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.country").exists())
                .andExpect(jsonPath("$.country").value("NO"))
                .andExpect(jsonPath("$.stations").exists())
                .andExpect(jsonPath("$.stations").value(1831))
                .andExpect(jsonPath("$.stationsWithPrices").exists())
                .andExpect(jsonPath("$.stationsWithPrices").value(1625))
                .andExpect(jsonPath("$.fuelStationBrands").exists())
                .andExpect(jsonPath("$.fuelStationBrands.length()").value(5));
    }

    @Test
    void shouldGETThreeTopBrands() throws Exception {
        //given
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/fuel-report/json")
                .param("limit", "3")
                .contentType(MediaType.APPLICATION_JSON_VALUE);

        //when
        ResultActions result = this.mockMvc.perform(requestBuilder);

        //then
        mockServer.verify();

        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.fuelStationBrands").exists())
                .andExpect(jsonPath("$.fuelStationBrands.length()").value(3));
    }
}